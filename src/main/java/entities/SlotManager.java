package entities;

import java.util.ArrayList;
import java.util.Date;

public class SlotManager {
    private static SlotManager slotManager;
    private ArrayList<SlotDetails>slotDetails = new ArrayList<SlotDetails>();
    public static SlotManager getInstance () {
        if (SlotManager.slotManager == null) {
            SlotManager.slotManager = new SlotManager();
            SlotManager.slotManager.addSlots();
        }
        return SlotManager.slotManager;
    }
    public void addSlots(){
        ArrayList<Sampler>samplers = new ArrayList<Sampler>();
        samplers.add(new Sampler("ali"));
        samplers.add(new Sampler("reza"));
        slotDetails.add(new SlotDetails(samplers, 1, new Date(6093550080000L)));
        samplers = new ArrayList<Sampler>();
        samplers.add(new Sampler("hossein"));
        samplers.add(new Sampler("mohammad"));
        samplers.add(new Sampler("ali"));
        samplers.add(new Sampler("reza"));
        slotDetails.add(new SlotDetails(samplers, 0, new Date(7093550080000L)));
        samplers = new ArrayList<Sampler>();
        samplers.add(new Sampler("mohammad"));
        slotDetails.add(new SlotDetails(samplers, 1, new Date(8093550080000L)));
    }

    public Sampler setSampler(Date date){
        for(SlotDetails slotDetail: slotDetails){
            if(slotDetail.getDate().equals(date)){
                if(slotDetail.getNumberOfKits() > 0 && slotDetail.getAvailableSamplers().size() > 0){

                    Sampler lazySampler = slotDetail.getAvailableSamplers().get(0);
                    int minimum = lazySampler.getCheckOuts().size();
                    for(Sampler sampler: slotDetail.getAvailableSamplers()){
                        if(sampler.getCheckOuts().size() < minimum) {
                            lazySampler = sampler;
                            minimum = sampler.getCheckOuts().size();
                        }
                    }
                    slotDetail.reserveKit();
                    slotDetail.reserveSampler(lazySampler.getName());
                    lazySampler.addTestSchedule(date);
                    return lazySampler;
                }
            }
        }
        return null;
    }

    public ArrayList<SlotDetails> getSlotDetails() {
        return slotDetails;
    }

    public ArrayList<Date> getSlotDates(){
        ArrayList<Date> availableDates = new ArrayList<Date>();
        for(SlotDetails slotDetail:slotDetails){
            if(slotDetail.getNumberOfKits() > 0 && slotDetail.getAvailableSamplers().size() > 0)
                availableDates.add(slotDetail.getDate());
        }
        return availableDates;
    }

}
