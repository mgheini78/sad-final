package entities;

import java.util.ArrayList;

public class InsuranceDescription {
    private InsuranceType type;
    private ArrayList<TestDescription> tests;

    public InsuranceDescription(InsuranceType type, ArrayList<TestDescription> tests){
        this.type = type;
        this.tests = tests;

    }

    public InsuranceType getType() {
        return type;
    }
}
