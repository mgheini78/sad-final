package entities;

import java.util.ArrayList;
import java.util.Date;

public class SlotDetails {
    private ArrayList<Sampler> availableSamplers;
    private int numberOfKits;
    private Date date;

    public SlotDetails(ArrayList<Sampler>availableSamplers, int numberOfKits, Date date){
        this.availableSamplers = availableSamplers;
        this.numberOfKits = numberOfKits;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public ArrayList<Sampler> getAvailableSamplers() {
        return availableSamplers;
    }

    public int getNumberOfKits() {
        return numberOfKits;
    }
    public void reserveKit(){
        numberOfKits -= 1;
    }
    public void reserveSampler(String samplerName){
        for(Sampler sampler:availableSamplers){
            if(sampler.getName().equals(samplerName)) {
                availableSamplers.remove(sampler);
                return;
            }
        }
    }
}
