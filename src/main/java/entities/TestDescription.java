package entities;

public class TestDescription {
    private String name = "";
    private Boolean prescribed;
    public TestDescription(String name, Boolean prescribed){
        this.name = name;
        this.prescribed = prescribed;
    }

    public String getName() {
        return name;
    }

    public Boolean getPrescribed() {
        return prescribed;
    }
}
