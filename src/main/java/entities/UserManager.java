package entities;

import java.util.ArrayList;
import java.util.Date;

public class UserManager {

    private ArrayList<User> users = new ArrayList<User>();
    private static UserManager userManager;
    private User currentUser;
    public static UserManager getInstance () {
        if (UserManager.userManager == null) {
            UserManager.userManager = new UserManager();
            UserManager.userManager.addUser();
        }
        return UserManager.userManager;
    }

    public void addUser(){
        ArrayList<TestDescription>prescribedTests = new ArrayList<TestDescription>();
        prescribedTests.add(new TestDescription("BloodTest", false));
        Insurance insurance = new Insurance(InsuranceManager.getInstance().getInsuration(InsuranceType.life),new Date());
        UserManager.userManager.currentUser =new User("ali", "1234", prescribedTests, insurance);
        users.add(currentUser);
    }

    public Boolean deleteUserTest(String testName){
        return this.currentUser.deleteTest(testName);
    }

    public ArrayList<String> getUserTestsName(){
        ArrayList<String>testsName = new ArrayList<String>();
        for(Test test: currentUser.getTests())
            testsName.add(test.getName());
        return testsName;
    }

    public Boolean addTest(String testName){
        for(TestDescription testDescription:TestManager.getInstance().getTests()){
            if(testDescription.getName().equals(testName)){
                if(testDescription.getPrescribed()){
                    if(currentUser.getPrescribedTests().contains(testDescription)) {
                        System.out.println("added successfully");
                        return true;
                    }
                    else{
                        System.out.println("this test needs prescription");
                        return false;
                    }
                }
                this.currentUser.addTest(new Test(testName));
                System.out.println("added successfully");
                return true;
            }
        }
        System.out.println("test name is unavailable");
        return false;
    }

    public ArrayList<Lab>getProperLabs(){
        return LabManager.getInstance().canDoTests(getUserTestsName());
    }

    public CheckOut createCheckOut(String location, float cost){
        this.currentUser.getBasket().setCheckOut(new CheckOut(location, cost));
        return currentUser.getBasket().getCheckOut();
    }

    public float costOfTests(String labName, boolean useInsurance){
        return LabManager.getInstance().calculateCost(labName,useInsurance, UserManager.getInstance().getUserTestsName()
                , currentUser.getInsurance().getInsuranceDescription().getType());
    }

    public void payComplete(){
        currentUser.getBasket().getCheckOut().setPaid(true);
    }

    public void setDate(Date date){
        for(SlotDetails slotDetail:SlotManager.getInstance().getSlotDetails()){
            if(slotDetail.getDate().equals(date)){
                currentUser.getBasket().getCheckOut().setSlotDate(date);
            }
        }
    }

    public Sampler setSampler(){
        Sampler sampler = SlotManager.getInstance().setSampler(currentUser.getBasket().getCheckOut().getSlotDate());
        currentUser.getBasket().getCheckOut().setSampler(sampler);
        return sampler;
    }

    public void clearBasket(){
        currentUser.clearBasket();
    }

    public String getCheckOutInfo(){
        CheckOut checkOut= currentUser.getBasket().getCheckOut();
        String info = "";
        info += "Date of sampling: " + checkOut.getSlotDate();
        info += "\nSampler name is: " + checkOut.getSampler().getName();
        info += "\nLocation of sampling: " + checkOut.getLocation();
        info += "\nCost paid: " + checkOut.getPayCost();
        return info;
    }
}
