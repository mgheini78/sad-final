package entities;

import java.util.HashMap;
import java.util.Map;

public class Lab {
    private String name = "";
    private HashMap<TestDescription, Integer> performableTests = new HashMap<TestDescription, Integer>();
    private HashMap<InsuranceType, Integer> supportedInsurances;
    public Lab(String name, HashMap<TestDescription, Integer> performableTests, HashMap<InsuranceType, Integer> supportedInsurances){
        this.name = name;
        this.performableTests = performableTests;
        this.supportedInsurances = supportedInsurances;
    }
    public Boolean CanDoTest(String testName) {
        for (Map.Entry<TestDescription,Integer> test :performableTests.entrySet()){
            if(test.getKey().getName().equals(testName))
                return true;
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public HashMap<TestDescription, Integer> getPerformableTests() {
        return performableTests;
    }

    public HashMap<InsuranceType, Integer> getSupportedInsurances() {
        return supportedInsurances;
    }
}
