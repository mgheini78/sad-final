package entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LabManager {
    private static LabManager labManager;
    private ArrayList<Lab> labs = new ArrayList<Lab>();
    public static LabManager getInstance () {
        if (LabManager.labManager == null) {
            LabManager.labManager = new LabManager();
            LabManager.labManager.addLabs();
        }
        return LabManager.labManager;
    }

    public void addLabs(){
        HashMap<TestDescription, Integer> testDescriptions = new HashMap<TestDescription, Integer>();
        HashMap<InsuranceType, Integer>insuranceTypes = new HashMap<InsuranceType, Integer>();
        insuranceTypes.put(InsuranceType.taaminejtemaE, 100);
        insuranceTypes.put(InsuranceType.life, 80);
        testDescriptions.put(new TestDescription("BloodTest", false), 2000);
        LabManager.labManager.labs.add(new Lab("hakim", testDescriptions, insuranceTypes));
        insuranceTypes = new HashMap<InsuranceType, Integer>();
        testDescriptions = new HashMap<TestDescription, Integer>();
        insuranceTypes.put(InsuranceType.taaminejtemaE, 100);
        testDescriptions.put(new TestDescription("BloodTest", false), 3000);
        testDescriptions.put(new TestDescription("UrineTest", false), 5000);
        LabManager.labManager.labs.add(new Lab("motahari", testDescriptions, insuranceTypes));
        insuranceTypes = new HashMap<InsuranceType, Integer>();
        testDescriptions = new HashMap<TestDescription, Integer>();
        testDescriptions.put(new TestDescription("BloodTest", false), 7000);
        testDescriptions.put(new TestDescription("UrineTest", false), 9000);
        testDescriptions.put(new TestDescription("StoolTest", true), 5000);
        insuranceTypes.put(InsuranceType.taaminejtemaE, 100);
        LabManager.labManager.labs.add(new Lab("saadi", testDescriptions, insuranceTypes));
        testDescriptions = new HashMap<TestDescription, Integer>();
        insuranceTypes = new HashMap<InsuranceType, Integer>();
        testDescriptions.put(new TestDescription("UrineTest", false), 9000);
        testDescriptions.put(new TestDescription("StoolTest", true), 5000);
        insuranceTypes.put(InsuranceType.health, 20);
        LabManager.labManager.labs.add(new Lab("razi", testDescriptions, insuranceTypes));
        testDescriptions = new HashMap<TestDescription, Integer>();
        insuranceTypes = new HashMap<InsuranceType, Integer>();
        insuranceTypes.put(InsuranceType.health, 20);
        insuranceTypes.put(InsuranceType.life, 50);
        testDescriptions.put(new TestDescription("Sputum", true), 10000);
        LabManager.labManager.labs.add(new Lab("valiAsr", testDescriptions, insuranceTypes));
    }

    public ArrayList<Lab> canDoTests(ArrayList<String>tests){
        ArrayList<Lab>availableLabs = new ArrayList<Lab>();
        boolean result;
        for(Lab lab:LabManager.labManager.labs){
            result = true;
            for(String test:tests){
                if(!lab.CanDoTest(test)) {
                    result = false;
                }
            }
            if(result)
                availableLabs.add(lab);
        }
        return availableLabs;
    }
    public float calculateCost(String labName, boolean userInsurance, ArrayList<String>tests, InsuranceType insuranceType){
        float cost = 0;
        int percent = 0;
        for(Lab lab:LabManager.labManager.labs){
            if(lab.getName().equals(labName)){
                if(lab.getSupportedInsurances().containsKey(insuranceType)){
                    percent = lab.getSupportedInsurances().get(insuranceType);
                    //System.out.println(percent);
                }
                //System.out.println(lab.getPerformableTests().size());
                cost = 0;
                for(String test:tests){
                    for(Map.Entry<TestDescription, Integer> f : lab.getPerformableTests().entrySet()){
                        if(f.getKey().getName().equals(test))
                            cost += f.getValue();
                    }
                }
                //System.out.println(cost);
                if(!userInsurance){
                    percent = 0;
                }
                return cost * (100 -  percent)/100;
            }
        }
        return cost;
    }
}
