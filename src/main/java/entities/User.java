package entities;

import java.util.ArrayList;

public class User {

    private String userName = "";
    private String password = "";
    private ArrayList<TestDescription> prescribedTests;
    private Insurance insurance;
    private Basket basket = new Basket();
    private ArrayList<Basket> cartHistory = new ArrayList<Basket>();

    public User(String userName, String password, ArrayList<TestDescription> prescribedTests, Insurance insurance){
        this.userName = userName;
        this.password = password;
        this.prescribedTests = prescribedTests;
        this.insurance = insurance;
    }

    public Boolean deleteTest(String testName){
        return this.basket.deleteTest(testName);
    }

    public void addTest(Test test){
        this.basket.addTest(test);
    }

    public void clearBasket(){
        this.cartHistory.add(basket);
        this.basket = new Basket();

    }

    public ArrayList<Test> getTests() {
        return this.basket.getTests();
    }

    public Basket getBasket() {
        return basket;
    }

    public ArrayList<TestDescription> getPrescribedTests() {
        return prescribedTests;
    }

    public Insurance getInsurance() {
        return insurance;
    }
}
