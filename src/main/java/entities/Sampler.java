package entities;


import java.util.ArrayList;
import java.util.Date;

public class Sampler {
    private String name = "";
    private ArrayList<Date>schedules = new ArrayList<Date>();
    public Sampler(String name ){
        this.name = name;
    }

    public ArrayList<Date> getCheckOuts() {
        return schedules;
    }

    public String getName() {
        return name;
    }
    public void addTestSchedule(Date date){
        schedules.add(date);
    }
}
