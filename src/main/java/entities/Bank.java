package entities;
import java.util.*;

public class Bank {
    private static Bank bank;

    public static Bank getInstance () {
        if (Bank.bank == null) {
            Bank.bank = new Bank();
            System.out.println("new Bank");
        }
        return Bank.bank;
    }

    public Boolean randomTransaction(){
        Random rand = new Random();
        return rand.nextBoolean();
    }

    public Boolean fixedTransaction(Boolean result){
        return result;
    }
}
