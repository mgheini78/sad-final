package entities;
import java.util.*;

public class CheckOut {
    private Date slotDate;
    private String userLocation = "";
    private Date creationDate = new Date();
    private float payCost = 0;
    private Boolean isPaid = false;
    private Lab lab = null;
    private Sampler sampler = null;
    private String location = "";

    public CheckOut(String location, float payCost){
        this.location = location;
        this.payCost = payCost;
    }
    public void setLab(Lab lab) {
        this.lab = lab;
    }

    public Boolean getPaid() {
        return isPaid;
    }

    public void setSampler(Sampler sampler) {
        this.sampler = sampler;
    }

    public void setPaid(Boolean paid) {
        isPaid = paid;
    }

    public void setPayCost(int payCost) {
        this.payCost = payCost;
    }

    public float getPayCost() {
        return payCost;
    }

    public void setSlotDate(Date slotDate) {
        this.slotDate = slotDate;
    }

    public Date getSlotDate() {
        return slotDate;
    }

    public Sampler getSampler() {
        return sampler;
    }

    public String getLocation() {
        return location;
    }
}
