package entities;

import java.util.ArrayList;

public class InsuranceManager {
    private static InsuranceManager insuranceManager;
    private ArrayList<InsuranceDescription>insuranceDescriptions = new ArrayList<InsuranceDescription>();
    public static InsuranceManager getInstance () {
        if (InsuranceManager.insuranceManager == null) {
            InsuranceManager.insuranceManager = new InsuranceManager();
            InsuranceManager.insuranceManager.addInsuranceDescriptions();
        }
        return InsuranceManager.insuranceManager;
    }

    public void addInsuranceDescriptions(){
        ArrayList<TestDescription>tests= new ArrayList<TestDescription>();
        tests.add(new TestDescription("BloodTest", false));
        tests.add(new TestDescription("UrineTest", false));
        InsuranceManager.insuranceManager.insuranceDescriptions.add(new InsuranceDescription(InsuranceType.health, tests));
        InsuranceManager.insuranceManager.insuranceDescriptions.add(new InsuranceDescription(InsuranceType.life, tests));
        tests.add(new TestDescription("StoolTest", true));
        tests.add(new TestDescription("Sputum", true));
        InsuranceManager.insuranceManager.insuranceDescriptions.add(new InsuranceDescription(InsuranceType.taaminejtemaE, tests));
        InsuranceManager.insuranceManager.insuranceDescriptions.add(new InsuranceDescription(InsuranceType.takmili, tests));
    }

    public InsuranceDescription getInsuration(InsuranceType insuranceType){
        for(InsuranceDescription insuranceDescription:insuranceDescriptions){
            if(insuranceDescription.getType().equals(insuranceType))
                return insuranceDescription;
        }
        return null;
    }

}
