package entities;
import java.util.*;

public class Basket {
    private ArrayList<Test> tests = new ArrayList<Test>();
    private CheckOut checkOut;

    public void addTest(Test test){
        this.tests.add(test);
    }

    public ArrayList<Test> getTests(){
        return this.tests;
    }

    public Boolean deleteTest(String testName){
        for(Test test:this.tests){
            if(test.getName().equals(testName)){
                this.tests.remove(test);
                return true;
            }
        }
        return false;
    }

    public CheckOut getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(CheckOut checkOut) {
        this.checkOut = checkOut;
    }
}
