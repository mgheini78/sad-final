package entities;

import java.util.ArrayList;

public class TestManager {
    private ArrayList<TestDescription>tests = new ArrayList<TestDescription>();

    private static TestManager testManager;

    public static TestManager getInstance () {
        if (TestManager.testManager == null) {
            TestManager.testManager = new TestManager();
            TestManager.testManager.addTest("BloodTest", false);
            TestManager.testManager.addTest("UrineTest", false);
            TestManager.testManager.addTest("StoolTest", true);
            TestManager.testManager.addTest("Sputum", true);
        }
        return TestManager.testManager;
    }


    public void addTest(String name, Boolean prescribed){
        TestDescription test = new TestDescription(name, prescribed);
        this.tests.add(test);
    }

    public ArrayList<TestDescription> getTests() {
        return tests;
    }
    public ArrayList<String> getTestsName() {
        ArrayList<String>testNames = new ArrayList<String>();
        for(TestDescription testDescription: TestManager.testManager.tests){
            testNames.add(testDescription.getName());
        }
        return testNames;
    }

    public TestDescription getTestWithName(String testName){
        for(TestDescription testDescription: TestManager.testManager.tests){
            if(testDescription.getName().equals(testName))
                return testDescription;
        }
        return null;
    }
}
