package entities;

import java.util.*;


public class Insurance {
    private InsuranceDescription insuranceDescription;
    private Date expiration_date = null;
    public Insurance(InsuranceDescription insuranceDescription, Date expiration_date){
        this.insuranceDescription = insuranceDescription;
        this.expiration_date = expiration_date;
    }

    public InsuranceDescription getInsuranceDescription() {
        return insuranceDescription;
    }
}
