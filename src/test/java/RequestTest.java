import entities.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class RequestTest {
//
    public Boolean runCommand(String command) {
        String[] arr = command.split(" ", 2);
        boolean result;
        if ("getTests".equals(arr[0])) {
            System.out.println(TestManager.getInstance().getTestsName());
        }

        else if ("addTest".equals(arr[0])) {
            if (arr.length == 1) {
                System.out.println(TestManager.getInstance().getTestsName());
                return false;
            }
            UserManager.getInstance().addTest(arr[1]);

        }
        else if ("quit".equals(arr[0])){
            System.out.println("request canceled");
            return true;
        }

        else if ("getBasket".equals(arr[0])) {
            System.out.println(UserManager.getInstance().getUserTestsName());
        }

        else if ("remove".equals(arr[0])) {
            if (arr.length == 1) {
                System.out.println(UserManager.getInstance().getUserTestsName());
                return false;
            }
            result = UserManager.getInstance().deleteUserTest(arr[1]);
            if (result)
                System.out.println("deleted successfully!");
            else
                System.out.println("there is not such this test name in basket!");
        }

        else if ("finalize".equals(arr[0])) {
            if(UserManager.getInstance().getUserTestsName().size() == 0){
                System.out.println("there is no test in cart for finalizing, please add some!");
                return false;
            }
            float cost = 0;

            boolean flag = false;
            while(!flag) {
                ArrayList<Lab> availableLabs = UserManager.getInstance().getProperLabs();
                System.out.println("choose one lab");
                ArrayList<String> labNames = new ArrayList<String>();
                for(Lab lab: availableLabs) {
                    labNames.add(lab.getName());
                }
                System.out.println(labNames);
                Scanner inputScanner = new Scanner(System.in);
                command = inputScanner.nextLine();
                if(command.equals("cancel")) {
                    System.out.println("request canceled");
                    return false;
                }
                if (!labNames.contains(command)) {
                    System.out.println("this lab is not available!");
                }
                else {
                    System.out.println("do you want to use insurance? (y/n)");
                    inputScanner = new Scanner(System.in);
                    String answer = inputScanner.nextLine();
                    if(answer.equals("cancel")) {
                        System.out.println("request canceled");
                        return false;
                    }
                    if(answer.equals("y")) {
                        cost = UserManager.getInstance().costOfTests(command, true);
                    }
                    else {
                        cost = UserManager.getInstance().costOfTests(command, false);
                    }
                    System.out.println("cost of tests is " + cost);
                    System.out.println("Do uou want choose another Lab? (y/n)");
                    inputScanner = new Scanner(System.in);
                    answer = inputScanner.nextLine();
                    if(answer.equals("cancel")) {
                        System.out.println("request canceled");
                        return false;
                    }
                    if(answer.equals("n"))
                        flag = true;

                }
            }
            flag = false;
            while(!flag){
                System.out.println("Enter your location");
                Scanner inputScanner = new Scanner(System.in);
                command = inputScanner.nextLine();
                if(command.equals("cancel")) {
                    System.out.println("request canceled");
                    return false;
                }
                if(!command.equals("")) {
                    CheckOut checkOut = UserManager.getInstance().createCheckOut(command, cost);
                    flag = true;
                }
                else
                    System.out.println("location can not be empty");
            }
            flag = false;
            while(!flag) {
                System.out.println("choose one slot number");
                ArrayList<Date> availabeDates = SlotManager.getInstance().getSlotDates();
                int counter = 0;
                for (Date date : availabeDates) {
                    System.out.print("" + counter + ":");
                    System.out.println(date);
                    counter += 1;
                }
                Scanner inputScanner = new Scanner(System.in);
                command = inputScanner.nextLine();
                if(command.equals("cancel")) {
                    System.out.println("request canceled");
                    return false;
                }
                if (command.equals("")) {
                    System.out.println("slot number can not be empty");
                } else {
                    try {
                        int d = Integer.parseInt(command);
                        if (d >= availabeDates.size())
                            System.out.println("out of range!");
                        else {
                            flag = true;
                            UserManager.getInstance().setDate(availabeDates.get(d));
                            System.out.print("slot reserved successfully");
                        }
                    } catch (NumberFormatException nfe) {
                        System.out.println("slot number should be number!");
                    }

                }
            }
            if(cost != 0) {
                flag = false;
                while (!flag) {
                    System.out.println("do you want to pay? (y/n)");
                    Scanner inputScanner = new Scanner(System.in);
                    command = inputScanner.nextLine();
                    if(command.equals("cancel")) {
                        System.out.println("request canceled");
                        return false;
                    }
                    if (!command.equals("y")) {
                        System.out.println("you didnt paid, so request later");
                        return true;
                    }
                    Bank bank = new Bank();
                    if (bank.randomTransaction()) {
                        UserManager.getInstance().payComplete();
                        System.out.println("paid successfully");
                        flag = true;

                    } else
                        System.out.println("transaction failed");
                }
            }
            Sampler sampler = UserManager.getInstance().setSampler();
            if(sampler == null) {
                System.out.println("there is no sampler or kit in this date, try later");
            }
            else {
                System.out.println("request for doing test completed!");
                System.out.println(UserManager.getInstance().getCheckOutInfo());
                UserManager.getInstance().clearBasket();
            }

        }
        else if ("getCommands".equals(arr[0])){
            System.out.println("getTests, getBasket, addTest, addTest sth, remove, remove sth, quit, finalize");
        }
        else {
            System.out.println("Command in not correct!");
        }
        return false;
    }
    public static void main(String[] args) {
        RequestTest requestTest = new RequestTest();

        while (true) {
            System.out.println("enter some command");
            String command;
            Scanner inputScanner = new Scanner(System.in);
            command = inputScanner.nextLine();
            if(requestTest.runCommand(command))
                break;

        }
    }
}
